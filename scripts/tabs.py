import re
import urllib 

pads = [
'http://10.10.161.238/ether/p/amazon/export/txt',
'http://10.10.161.238/ether/p/polyhistorm/export/txt',
'http://10.10.161.238/ether/p/etymologiae/export/txt',
'http://10.10.161.238/ether/p/cyclopaedia/export/txt',
'http://10.10.161.238/ether/p/encyclopedia/export/txt',
'http://10.10.161.238/ether/p/great-inventions/export/txt',
'http://10.10.161.238/ether/p/taric/export/txt'
]


with open("../../stories/systems.html","w+") as fileout:
	for pad in pads:

		lines = urllib.urlopen(pad).readlines()
		padname = pad.replace('http://10.10.161.238/ether/p/','')
		padname = padname.replace('/export/txt','')

		fileout.write('<div id="main">')

		for line in lines: 
			line = re.sub(r"\s{8}", "\t", line)
			m = re.search(r"^\t*", line) # for tabs

			# m = re.search(r"\s{8}", line) # for 8 spaces as 1 tab
			if m:
				tab = len(m.group(0)) + 1
				strtab = str(tab)
			line = line.replace("\t","")

			if '$!' in line:
				line = line.replace('$!','<span class="annotation">')
				line = line+'</span>'

			fontsize = 55/tab
			fontsize = str(fontsize)
			# fontsize = str(12); # uncomment to set fixed fontsize 

			fileout.write('<div class="h'+strtab+'" style="font-size:'+fontsize+'px;">'+line+'</div>\n')

		fileout.write("""
			</div>
			""")

	print '*output of systems.html is written*'

	fileout.close()