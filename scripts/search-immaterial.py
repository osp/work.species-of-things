import re
import urllib
import argparse

local = 'yes'

if local == 'yes':
	pads = [
		'../classification-systems/encyclopedie-updated.txt',
		'../classification-systems/cyclopaedia.txt',
		'../classification-systems/greatinventions.txt',
		'../classification-systems/polyhistor-updated.txt',
		'../classification-systems/etymologiae.txt',
		'../classification-systems/taric.txt',
		'../classification-systems/amazon.txt'
	]
	fileout = open("../html2print/stories/immaterial.html","w+")
else:
	pads = [
		'http://10.10.161.238/ether/p/polyhistor/export/txt',
		'http://10.10.161.238/ether/p/etymologiae/export/txt',
		'http://10.10.161.238/ether/p/cyclopaedia/export/txt',
		'http://10.10.161.238/ether/p/encyclopedia/export/txt',
		'http://10.10.161.238/ether/p/greatinventions/export/txt',
		'http://10.10.161.238/ether/p/taric/export/txt',
		'http://10.10.161.238/ether/p/amazon/export/txt'
	]
	fileout = open("../../stories/immaterial.html","w+")

fileout.write("""
	<style type="text/css" media="all">
		html {
			background: white;
			font-size: 10pt;
			font-family:'sans-serif';
		}

		.column {
			width: calc(100% / 8 - 10px);
			float: left;
		}

		h1, p {
			hyphens: auto;
			-webkit-hyphens: auto;
		}
		h1{
			font-size:12pt;
			line-height:1;
		}

		.column + .column {
			margin-left: 10px;
		}

		p {
			opacity: .1;
		}

		.immaterial {
			opacity: 1;
		}

	</style>
""")

for pad in pads:

	c = 0

	lines = urllib.urlopen(pad).readlines()
	padname = pad.replace('../classification-systems/','')
	padname = padname.replace('.txt','')
	padname = padname.replace('http://10.10.161.238/ether/p/','')
	padname = padname.replace('/export/txt','')

	fileout.write('<div class="column">')

	fileout.write('<h1 lang="en-us">{}</h1>'.format(padname))

	for line in lines:
		line = re.sub('\s{8}', '\t', line)
		m = re.search(r"^\t*", line)
		if m:
			tab = len(m.group(0)) + 1
			strtab = str(tab)
		line = line.replace("\t","")

		
		if '[immaterial]' in line:
			fileout.write('<p class="immaterial"  lang="en-us">{}</p>'.format(line.replace('[immaterial]', '')))
			continue
		# else:
			# fileout.write('<p lang="en-us">{}</p>'.format(line.replace('[immaterial]', '')))
			# continue

	fileout.write('</div>');
print '*output written to immaterial.html*'

fileout.close()
