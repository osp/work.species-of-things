import re
import urllib
import argparse

local = 'yes'

if local == 'yes':
	pads = [
		'../classification-systems/polyhistor.txt',
		'../classification-systems/etymologiae.txt',
		'../classification-systems/cyclopaedia.txt',
		'../classification-systems/encyclopedia.txt',
		'../classification-systems/greatinventions.txt',
		'../classification-systems/taric.txt',
		'../classification-systems/amazon.txt'
	]
	fileout = open("../html2print/stories/boxes.html","w+")
else:
	pads = [
		'http://10.10.161.238/ether/p/polyhistor/export/txt',
		'http://10.10.161.238/ether/p/etymologiae/export/txt',
		'http://10.10.161.238/ether/p/cyclopaedia/export/txt',
		'http://10.10.161.238/ether/p/encyclopedia/export/txt',
		'http://10.10.161.238/ether/p/greatinventions/export/txt',
		'http://10.10.161.238/ether/p/taric/export/txt',
		'http://10.10.161.238/ether/p/amazon/export/txt'
	]
	fileout = open("../../stories/boxes.html","w+")

for pad in pads:

	c = 0

	lines = urllib.urlopen(pad).readlines()
	padname = pad.replace('../classification-systems/','')
	padname = padname.replace('.txt','')
	padname = padname.replace('http://10.10.161.238/ether/p/','')
	padname = padname.replace('/export/txt','')

	for line in lines:
		line = re.sub('\s{8}', '\t', line)
		m = re.search(r"^\t*", line)
		if m:
			tab = len(m.group(0)) + 1
			strtab = str(tab)
		line = line.replace("\t","")

		if '[annotation_ts]' in line:
			line = line.replace('[annotation_ts] ','<span class="annotation_ts">')
			line = line+'</span>'
			fileout.write(line)
			continue

		if '[title]' in line:
			line = line.replace('[title]','<div class="title">')
			line = line+'</div>'
			fileout.write(line)
			continue	

		if len(line.split(' ')) > 10:
			words = line.split(' ')
			line = ' '.join(words[:10]) + ' ...'
	
		if '[img]' in line:
			line = line.replace('[img] ','<span class="annotation"><img class="img-annotation" src="/html2print/chapters/img/')
			line = line+'"></span>'
			fileout.write(line)
			continue

		if '[annotation]' in line:
			line = line.replace('[annotation]','<span class="citation">')
			line = line+'</span>'
			fileout.write(line)
			continue							

		if tab == c:
			fileout.write('</div>\n')
			fileout.write('<div class="box level_'+strtab+'">'+line)
			# print 'tab is same'
			# print 'c:', c, 'tab:', tab
		
		if tab > c:
			fileout.write('<div class="box level_'+strtab+'">'+line)
			# print 'tab is bigger'
			# print 'c:', c, 'tab:', tab

		if tab < c:
			enddiv = '</div>\n'
			# print str(c - tab + 1)+ 'x enddiv printed'
			fileout.write((c - tab + 1) * enddiv)
			fileout.write('<div class="box level_'+strtab+'">'+line)
			# print 'tab is smaller'
			# print 'c:', c, 'tab:', tab
		
		c = tab

	fileout.write((tab + 1) * enddiv)

print '*output of boxes.html is written*'
fileout.close()
