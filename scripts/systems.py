import re
import urllib 

pads = [
'http://10.10.161.238/ether/p/polyhistor/export/txt',
'http://10.10.161.238/ether/p/etymologiae/export/txt',
'http://10.10.161.238/ether/p/cyclopaedia/export/txt',
'http://10.10.161.238/ether/p/encyclopedia/export/txt',
'http://10.10.161.238/ether/p/greatinventions/export/txt',
# 'http://10.10.161.238/ether/p/taric/export/txt',
# 'http://10.10.161.238/ether/p/amazon/export/txt'
]

# pads = [
# '../classification-systems/encyclopedia.txt',
# '../classification-systems/cyclopaedia.txt',
# '../classification-systems/greatinventions.txt',
# '../classification-systems/polyhistor.txt',
# '../classification-systems/etymologiae.txt',
# '../classification-systems/taric.txt'
# '../classification-systems/amazon.txt'
# ]

fileout = open("../../stories/systems.html","w+")
# fileout = open("../html2print/stories/systems.html","w+")

tmp = []

for pad in pads:

	lines = urllib.urlopen(pad).readlines()
	padname = pad.replace('../classification-systems/','')
	padname = padname.replace('.txt','')
	padname = padname.replace('http://10.10.161.238/ether/p/','')
	padname = padname.replace('/export/txt','')

	tmp.append('<div class="source">')
	tmp.append('<div class="title">'+padname+'</div>')

	for line in lines: 
		line = re.sub(r"\s{8}", "\t", line)
		m = re.search(r"^\t*", line) 

		if m:
			tab = len(m.group(0)) + 1
			strtab = str(tab)
		line = line.replace("\t","")

		if '!?' in line:
			line = line.replace('!?','<span class="annotation">')
			line = line+'</span>'

		# fontsize = 55/tab # commented out to style the fontsize in css
		# fontsize = str(fontsize) # commented out to style the fontsize in css
		# fontsize = str(12); # uncomment to set fixed fontsize 

		tmp.append('<div class="item h'+strtab+'">'+line+'</div>\n')

	tmp.append("""
		</div>
		""")

	print '*output of system-'+padname+'.html is written*'

tmp = ' '.join(tmp)
fileout.write(tmp)

fileout.close()