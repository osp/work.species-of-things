import re

filein = open("../classification-systems/diderot-et-d_alembert.txt","r")
with open("schets-manetta4_diderot.html","w+") as fileout:

	fileout.write('<html><head><link href="schets-manetta4.css" rel="stylesheet"></head><body><div id="main">')

	c = 1

	for line in filein: 
		m = re.search(r"^\t*", line)
		if m:
			tab = len(m.group(0)) + 1
			strtab = str(tab)
		line = line.replace("\t","")
		print line

		fontsize = str(10);

		if line == 'For slaughter\n':
			line = line+"<img src='img/Stages_of_Life_by_Bartholomeus_Anglicus_1486.jpg'>"
		if line == 'Bulls of the Schwyz, Fribourg and spotted Simmental breeds\n':
			line = line+"<img src='img/https---s3.amazonaws.com-ul-hos-legacy-images-tiff-02LateAncient-Solinus-1543-Solinus-1543det-007-detail.jpg'>"

		if tab == c:
			if "MEMOIRE: HISTOIRE\n" != line:
				fileout.write('</div>\n')
			fileout.write('<div class="h'+strtab+'">'+line)
			print 'tab is same'
			print 'c:', c, 'tab:', tab
		
		if tab > c:
			fileout.write('<div class="h'+strtab+'">'+line)
			print 'tab is bigger'
			print 'c:', c, 'tab:', tab

		if tab < c:
			enddiv = '</div>\n'
			fileout.write((c - tab + 1) * enddiv)
			print str(c - tab + 1)+ 'x enddiv printed'
			fileout.write('<div class="h'+strtab+'">'+line)
			print 'tab is smaller'
			print 'c:', c, 'tab:', tab

		c = tab
		
		print '**********************'

	fileout.write("""
		</div>
		</body>
		</html>
		""")

	fileout.close()