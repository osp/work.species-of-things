import re
import urllib
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('input', type=str, help="The text-file to parse")
parser.add_argument('output', type=str, help="The dot-file to store produced dot in.")
args = parser.parse_args()

is_url_match = r'^http(?:s)?:\/\/'

filein = urllib.urlopen(args.input) if re.match(is_url_match, args.input) else open(args.input, 'r')
with open(args.output, 'w') as fileout:

	fileout.write('<div id="main">')

	c = 1

	for line in filein:
		line = re.sub('\s{8}', '\t', line)
		m = re.search(r"^\t*", line)
		if m:
			tab = len(m.group(0)) + 1
			strtab = str(tab)
		line = line.replace("\t","")
		# print line

		fontsize = str(10);

		if tab == c:
			if "LIVE ANIMALS\n" != line:
				fileout.write('</div>\n')
			fileout.write('<div class="h'+strtab+'">'+line)
			# print 'tab is same'
			# print 'c:', c, 'tab:', tab
		
		if tab > c:
			fileout.write('<div class="level_'+strtab+'">'+line)
			# print 'tab is bigger'
			# print 'c:', c, 'tab:', tab

		if tab < c:
			enddiv = '</div>\n'
			fileout.write((c - tab + 1) * enddiv)
			# print str(c - tab + 1)+ 'x enddiv printed'
			fileout.write('<div class="h'+strtab+'">'+line)
			# print 'tab is smaller'
			# print 'c:', c, 'tab:', tab

		c = tab
		
		# print '**********************'

	fileout.write('</div>')
	fileout.close()