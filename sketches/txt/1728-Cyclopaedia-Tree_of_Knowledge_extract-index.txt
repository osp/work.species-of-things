KNOWLEDGE, is either
	Natural and Scientifical, which is either
		Sensible; consisting in ther Perception of Phaenomena, or External Objects—called PHYSIOLOGY, or NATURAL HISTORY; and which according to the different Kinds of such Objects, divides into
			METEOROLOGY.
			HYDROLOGY. 
			MINEROLOGY.
			PHYTOLOGY.
			ZOOLOGY.
		Rational; consisting in the Perception of the intrinsick Characters or Habitutes of sensible Objects—either
			Their Powers and Properties—called PHYSICKS and NATURAL PHILOSOPHY.
			Abstracts thereof—called METAPHYSICS—which subdivides into
				ONTOLOGY.
				PNEUMATOLOGY.
			Quantities thereof—called MATHEMATICS—which divides, according to the Subject of the Quantity, into
				ARITHMIC
					ANALYTICS.
					ALGEBRA.
				GEOMETRY
					TRIGONOMETRY.
					CONIGS.
					SPHERICS.
				STATICS
			Relations thereof to our Happiness—called RELIGION, or the Doctrine of OFFICES, which subdivides into—
				ETHICS, or NATURAL RELIGION—whence
					POLITICS.
					LAW.
				THEOLOGY, or RELEVATION.
	Artificial and Technical, (consisting in the Application of Natural Notices to further Purposes) which is either
		Internal; employ'd in discovering their Agreement and Disagreement; or their Relations in respect of Truth—call'd LOGICS.
		External, which is either—
			Real, employ'd in discovering and applying the—
				Futher Powers and Properties of Bodies—called CHYMISTRY—whence
					ALCHEMY.
					NATURAL MAGIC, etc.
				Quantities of Bodies—call'd MIX'D MATHEMATICS; which according to the different Subjects resolves into
					OPTICS, CATOPTRICS, DIOPTRICS—whence
						PERSPECTIVE.
						PAINTING.
					PHONIC—whence
						MUSICK.
					HYDROSTATICS, HYDRAULICS.
					PNEUMATICS.
					MECHANICS—whence
						ARCHITECTURE.
						SCULPTURE.
						TRADES, and MANUFACTURES.
					PYROTECHNIA—whence
						THE MILLITARY Art.
						FORTIFICATION.
					ASTRONOMY—whence
						CHRONOLOGY.
						DIALLING.
					GEOGRAPHY, HYDROGRAPHY—whence
						NAVIGATION.
						COMMERCE.
				Structure and Oeconomy of Organical Bodies—called ANATOMY.
				Relations thereof to the Preservation and Improvement—either of
					Animals—called
						MEDICINE.
						PHARMACY.
					Vegetables—called
						AGRICULTURE.
						GARDENING.
					Brutes—called
						FARRYING, MANAGE—whence
							HUNTING.
							FALCONRY.
							FISHING, etc.
			Symbolical, employ'd in framing
				Words, or Articulate Signs of Ideas—called GRAMMAR.
				Figures—called RHETORIC—whence
					The making of Armories, called HERALDRY.
				Fables—called POETRY.