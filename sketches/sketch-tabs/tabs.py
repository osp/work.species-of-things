import re

# filein = open("../txt/2013-TARIC-nomenclature_index.txt","r")

# read from an etherpad
import urllib 
filein = urllib.urlopen('http://10.10.161.238/ether/p/encyclopedia/export/txt')

with open("output/tabs-encyclopedia.html","w+") as fileout:

	fileout.write('<html><head><link href="../css/tabs.css" rel="stylesheet"></head><body><div id="main">')

	for line in filein: 
		line = re.sub(r"\s{8}", "\t", line)
		print line
		m = re.search(r"^\t*", line) # for tabs

		# m = re.search(r"\s{8}", line) # for 8 spaces as 1 tab
		if m:
			tab = len(m.group(0)) + 1
			strtab = str(tab)
		line = line.replace("\t","")
		# print line, tab

		fontsize = 55/tab
		fontsize = str(fontsize)
		fontsize = str(12); # uncomment to set fixed fontsize 

		fileout.write('<div class="h'+strtab+'" style="font-size:'+fontsize+'px;">'+line+'</div>\n')
		

	fileout.write("""
		</div>
		</body>
		</html>
		""")

	fileout.close()