import re

filein = open("../classification-systems/diderot-et-d_alembert.txt","r")
with open("schets-manetta3_diderot.html","w+") as fileout:

	fileout.write('<html><head><link href="schets-manetta3_diderot.css" rel="stylesheet"></head><body><div id="main">')

	for line in filein: 
		m = re.search(r"^\t*", line)
		if m:
			tab = len(m.group(0)) + 1
			strtab = str(tab)
		line = line.replace("\t","")
		print line, tab

		fontsize = 60/tab
		fontsize = str(fontsize)
		fontsize = str(16);

		fileout.write('<div class="h'+strtab+'" style="font-size:'+fontsize+'px;">'+line+'</div>\n')
		

	fileout.write("""
		</div>
		</body>
		</html>
		""")

	fileout.close()