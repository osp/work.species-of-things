[title] 2016 - EU Taric system
Live animals; animal products 
        Live animals 
        Meat and edible meat offal 
        Fish and crustaceans, molluscs and other aquatic invertebrates 
        Dairy produce; birds' eggs; natural honey; edible products of animal origin, not elsewhere specified or included 
        Products of animal origin, not elsewhere specified or included 
Vegetable products 
        Live trees and other plants; bulbs, roots and the like; cut flowers and ornamental foliage 
        Edible vegetables and certain roots and tubers 
        Edible fruit and nuts; peel of citrus fruit or melons 
        Coffee, tea, maté and spices 
        Cereals 
        Products of the milling industry; malt; starches; inulin; wheat gluten 
        Oil seeds and oleaginous fruits; miscellaneous grains, seeds and fruit; industrial or medicinal plants; straw and fodder 
        Lac; gums, resins and other vegetable saps and extracts 
        Vegetable plaiting materials; vegetable products not elsewhere specified or included 
Animal or vegetable fats and oils and their cleavage products; prepared edible fats; animal or vegetable waxes 
        Animal or vegetable fats and oils and their cleavage products; prepared edible fats; animal or vegetable waxes 
Prepared foodstuffs; beverages, spirits and vinegar; tobacco and manufactured tobacco substitutes 
        Preparations of meat, of fish or of crustaceans, molluscs or other aquatic invertebrates 
        Sugars and sugar confectionery 
        Cocoa and cocoa preparations 
        Preparations of cereals, flour, starch or milk; pastrycooks' products 
        Preparations of vegetables, fruit, nuts or other parts of plants 
        Miscellaneous edible preparations 
        Beverages, spirits and vinegar 
        Residues and waste from the food industries; prepared animal fodder 
        Tobacco and manufactured tobacco substitutes 
Mineral products 
        Salt; sulphur; earths and stone; plastering materials, lime and cement 
        Ores, slag and ash 
        Mineral fuels, mineral oils and products of their distillation; bituminous substances; mineral waxes 
Products of the chemical or allied industries 
        Inorganic chemicals; organic or inorganic compounds of precious metals, of rare-earth metals, of radioactive elements or of isotopes 
        Organic chemicals 
        Pharmaceutical products 
        Fertilisers 
        Tanning or dyeing extracts; tannins and their derivatives; dyes, pigments and other colouring matter; paints and varnishes; putty and other mastics; inks 
        Essential oils and resinoids; perfumery, cosmetic or toilet preparations 
        Soap, organic surface-active agents, washing preparations, lubricating preparations, artificial waxes, prepared waxes, polishing or scouring preparations, candles and similar articles, modelling pastes, ‘dental waxes’ and dental preparations with a basis of plaster 
        Albuminoidal substances; modified starches; glues; enzymes 
        Explosives; pyrotechnic products; matches; pyrophoric alloys; certain combustible preparations 
        Photographic or cinematographic goods 
        Miscellaneous chemical products 
Plastics and articles thereof; rubber and articles thereof 
        Plastics and articles thereof 
        Rubber and articles thereof 
Raw hides and skins, leather, furskins and articles thereof; saddlery and harness; travel goods, handbags and similar containers; articles of animal gut (other than silkworm gut) 
        Raw hides and skins (other than furskins) and leather 
        Articles of leather; saddlery and harness; travel goods, handbags and similar containers; articles of animal gut (other than silkworm gut) 
        Furskins and artificial fur; manufactures thereof 
Wood and articles of wood; wood charcoal; cork and articles of cork; manufactures of straw, of esparto or of other plaiting materials; basketware and wickerwork 
        Wood and articles of wood; wood charcoal 
        Cork and articles of cork 
        Manufactures of straw, of esparto or of other plaiting materials; basketware and wickerwork 
Pulp of wood or of other fibrous cellulosic material; recovered (waste and scrap) paper or paperboard; paper and paperboard and articles thereof 
        Pulp of wood or of other fibrous cellulosic material; recovered (waste and scrap) paper or paperboard 
        Paper and paperboard; articles of paper pulp, of paper or of paperboard 
        Printed books, newspapers, pictures and other products of the printing industry; manuscripts, typescripts and plans 
Textiles and textile articles 
        Silk 
        Wool, fine or coarse animal hair; horsehair yarn and woven fabric 
        Cotton 
        Other vegetable textile fibres; paper yarn and woven fabrics of paper yarn 
        Man-made filaments; strip and the like of man-made textile materials 
        Man-made staple fibres 
        Wadding, felt and nonwovens; special yarns; twine, cordage, ropes and cables and articles thereof 
        Carpets and other textile floor coverings 
        Special woven fabrics; tufted textile fabrics; lace; tapestries; trimmings; embroidery 
        Impregnated, coated, covered or laminated textile fabrics; textile articles of a kind suitable for industrial use 
        Knitted or crocheted fabrics 
        Articles of apparel and clothing accessories, knitted or crocheted 
        Articles of apparel and clothing accessories, not knitted or crocheted 
        Other made-up textile articles; sets; worn clothing and worn textile articles; rags 
Footwear, headgear, umbrellas, sun umbrellas, walking sticks, seat-sticks, whips, riding-crops and parts thereof; prepared feathers and articles made therewith; artificial flowers; articles of human hair 
        Footwear, gaiters and the like; parts of such articles 
        Headgear and parts thereof 
        Umbrellas, sun umbrellas, walking sticks, seat-sticks, whips, riding-crops and parts thereof 
        Prepared feathers and down and articles made of feathers or of down; artificial flowers; articles of human hair 
Articles of stone, plaster, cement, asbestos, mica or similar materials; ceramic products; glass and glassware 
        Articles of stone, plaster, cement, asbestos, mica or similar materials 
        Ceramic products 
        Glass and glassware 
Natural or cultured pearls, precious or semi-precious stones, precious metals, metals clad with precious metal, and articles thereof; imitation jewellery; coin 
        Natural or cultured pearls, precious or semi-precious stones, precious metals, metals clad with precious metal, and articles thereof; imitation jewellery; coin 
Base metals and articles of base metal 
        Iron and steel 
        Articles of iron or steel 
        Copper and articles thereof 
        Nickel and articles thereof 
        Aluminium and articles thereof 
        (Reserved for possible future use in the Harmonised System) 
        Lead and articles thereof 
        Zinc and articles thereof 
        Tin and articles thereof 
        Other base metals; cermets; articles thereof 
        Tools, implements, cutlery, spoons and forks, of base metal; parts thereof of base metal 
        Miscellaneous articles of base metal 
Machinery and mechanical appliances; electrical equipment; parts thereof; sound recorders and reproducers, television image and sound recorders and reproducers, and parts and accessories of such articles 
        Nuclear reactors, boilers, machinery and mechanical appliances; parts thereof 
        Electrical machinery and equipment and parts thereof; sound recorders and reproducers, television image and sound recorders and reproducers, and parts and accessories of such articles 
Vehicles, aircraft, vessels and associated transport equipment 
        Railway or tramway locomotives, rolling stock and parts thereof; railway or tramway track fixtures and fittings and parts thereof; mechanical (including electromechanical) traffic signalling equipment of all kinds 
        Vehicles other than railway or tramway rolling stock, and parts and accessories thereof 
        Aircraft, spacecraft, and parts thereof 
        Ships, boats and floating structures 
Optical, photographic, cinematographic, measuring, checking, precision, medical or surgical instruments and apparatus; clocks and watches; musical instruments; parts and accessories thereof 
        Optical, photographic, cinematographic, measuring, checking, precision, medical or surgical instruments and apparatus; parts and accessories thereof 
        Clocks and watches and parts thereof 
        Musical instruments; parts and accessories of such articles 
Arms and ammunition; parts and accessories thereof 
        Arms and ammunition; parts and accessories thereof 
Miscellaneous manufactured articles 
        Furniture; bedding, mattresses, mattress supports, cushions and similar stuffed furnishings; lamps and lighting fittings, not elsewhere specified or included; illuminated signs, illuminated nameplates and the like; prefabricated buildings 
        Toys, games and sports requisites; parts and accessories thereof 
        Miscellaneous manufactured articles 
Works of art, collectors' pieces and antiques 
        Works of art, collectors' pieces and antiques 
        Complete industrial plant 
        Special Combined Nomenclature codes
