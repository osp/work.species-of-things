[title] 1751 - Diderot and d'Alembert - Encyclopedie
understanding
        memory: history
                sacred (history of prophets)
                ecclesiastical
                civil, ancient & modern
                        civil history, properly said, literary history
                                memories
                                antiquities
                                [img] Encyclopedie-Antiquities.jpg
                                complete histories
                natural
                        uniformity of nature
                                celestial history
                                history
                                        of meteors
                                        of land and sea
                                        of minerals
                                        of plants
                                        of animals
                                        of elements
                        deviations of nature
                                celestial wonders
                                large meteors
                                wonders on land and sea
                                monstrous minerals
                                monstrous plants
                                monstrous animals
                                wonders of the elements
                        uses of nature: arts, crafts, manufactures
                                work and uses of gold and silver
                                        minting
                                        goldsmith
                                        [img] Encyclopedie-Goldsmith.jpg
                                        gold spinning
                                        gold drawing
                                        silversmith
                                        planisher
                                work and uses of precious stones
                                        lapidary
                                        [img] Encyclopedie-Lapidary.jpg
                                        diamond cutting
                                        jeweller
                                work and uses of iron
                                        large forges
                                        [img] Encyclopedie-LargeForges.jpg
                                        locksmith
                                        tool making
                                        armorer
                                        gun-making
                                        [img] Encyclopedie-Gunsmith.jpg
                                work and uses of glass
                                        glass making
                                        [img] Encyclopedie-GlassMaking.jpg
                                        plate-glass making
                                        mirror making
                                        optician
                                        glazier
                                work and uses of skin
                                [img] Encyclopedie-Skins.jpg
                                        tanner
                                        shammy-maker
                                        leather merchant
                                        glove making
                                work and uses of stone, plaster, slate, etc.
                                        practical architecture
                                        practical sculpture
                                        mason
                                        [img] Encyclopedie-Masonry.jpg
                                        tiler
                                work and uses of silk
                                        spinning
                                        milling
                                        [img] Encyclopedie-Silk.jpg
                                        work
                                        velvet
                                        brocaded fabrics
                                work and uses of wool
                                        cloth-making
                                        [img] Encyclopedie-ClothMaking.jpg
                                        bonnet-making
                                work and uses, etc.
        reason: philosophy
                general metaphysics, or ontology, or science of being in general, of possibility, of existence, of duration, etc.
                science of God
                        natural theology and revealed theology
                                religion, whereby, through abuse, supersitition
                                [img] Encyclopedie-Religion.jpg
                        science of good and evil spirits
                                divination
                                black magic
                science of man
                        pneumatology or science of the soul
                                reasonable
                                sensible
                        logic
                                art of thinking
                                        apprehension: science of ideas
                                        judgment: science of propositions
                                        reasoning: induction
                                        method: demonstration
                                                analysis
                                                synthesis
                                art of remembering
                                        memory
                                                natural
                                                artificial
                                                        prenotion
                                                        emblem
                                        supplement to memory
                                                writing and printing
                                                [img] Encyclopedie-Writing.jpg
                                                        alphabet
                                                        cipher
                                                                arts of writing, printing, reading, deciphering: orthography
                                art of communicating
                                        science of the instrument of discourse: grammar
                                                signs
                                                        gesture
                                                                pantomime
                                                                declamation
                                                        character
                                                                ideograms
                                                                hieroglyphics
                                                                heraldry
                                                                [img] Encyclopedie-Heraldry.jpg
                                                                blazonry
                                                prosody
                                                construction
                                                syntax
                                                philology
                                                critique
                                                pedagogy
                                                        choice of studies
                                                        manner of teaching
                                        science of the qualities of discourse
                                                rhetorics
                                                mechanics of poetry
                        ethics
                                general
                                        general science of good and evil, of duties in general, of virtue, of the necessity of being virtuous, etc.
                                particular
                                        science of laws and jurisprudence
                                                natural
                                                economic and political
                                                        internal and external commerce on land and sea
                science of nature
                        metaphysics of bodies, or general physics, of extent, of impenetrability, of movement, of word, etc.
                        mathematics
                                pure
                                        arithmetic
                                                numeric
                                                algebra
                                                [img] Encyclopedie-Algebra.jpg
                                                        elementary
                                                        infinitesimal
                                                                differential
                                                                integral
                                        geometry
                                                elementary: military architecture, tactics
                                                transcendental: theory of courses
                                mixed
                                        mechanics
                                        [img] Encyclopedie-Mechanics.jpg
                                                statics
                                                        statics, properly said, hydrostatics
                                                dynamics
                                                        dynamics, properly said, ballistics
                                                        hydrodynamics
                                                                hydraulics
                                                                [img] Encyclopedie-Hydraulics.jpg
                                                                navigation
                                                                naval architecture
                                                                [img] Encyclopedie-Ship.jpg
                                        geometric astronomy
                                        [img] Encyclopedie-Astronomy.jpg
                                                cosmography
                                                        uranography
                                                        geography
                                                        hydrography
                                                chronology
                                                gnomon
                                        optics
                                        [img] Encyclopedie-Optics.jpg
                                                optics, properly said, dioptrics
                                                perspective
                                                catoptrics
                                        acoustics
                                        pneumatics
                                        [img] Encyclopedie-Pneumatics.jpg
                                        art of conjecture
                                        analysis of chance
                                physicomathematics
                        particular physics
                                zoology
                                        anatomy
                                        [img] Encyclopedie-Anatomy.jpg
                                                simple
                                                comparative
                                        physiology
                                        medicine
                                                hygiene
                                                        hygiene, properly said, cosmetics
                                                        orthopaedics
                                                        athletics
                                                        gymnastics
                                                pathology
                                                semiotics
                                                treatment
                                                        diet
                                                        surgery
                                                        [img] Encyclopedie-Surgery.jpg
                                                        pharmacy
                                        veterinary
                                        horse management
                                        [img] Encyclopedie-Horses.jpg
                                        hunting
                                        fishing
                                        [img] Encyclopedie-Fishing.jpg
                                        falconry
                                physical astronomy: astrology
                                        judiciary astrology
                                        physical astrology
                                meteorology
                                cosmology
                                [img] Encyclopedie-Physics.jpg
                                        uranology
                                        aerology
                                        geology
                                        hydrology
                                botany
                                        agriculture
                                        gardening
                                        [img] Encyclopedie-Gardening.jpg
                                mineralogy
                                chemistry
                                        chemistry, properly said, pyrotechnics
                                        [img] Encyclopedie-Chemistry.jpg
                                        dyeing
                                        metallurgy
                                        [img] Encyclopedie-Metallurgy.jpg
                                        alchemy
                                        natural magic
        imagination
                poetry, sacred and profane
                        narrative
                                epic poetry
                                madrigal
                                epigram
                                novel
                        dramatic
                                tragedy
                                comedy
                                pastoral
                        parable
                                allegory
                music
                        theory
                        practice
                        instrumental
                        [img] Encyclopedie-Music.jpg
                        vocal
                painting
                sculpture
                [img] Encyclopedie-Sculpture.jpg
                civil
                architecture
                [img] Encyclopedie-Architecture.jpg
                engraving
                [img] Encyclopedie-Engraving.jpg

