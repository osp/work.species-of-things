// A re-implementation of "mutool merge" in JavaScript.

function graftObject(dstDoc, srcDoc, srcObj, map) {
	var srcNum, dstRef, dstObj
	if (!map)
		map = []
	if (srcObj.isIndirect()) {
		srcNum = srcObj.toIndirect()
		if (map[srcNum])
			return map[srcNum]
		map[srcNum] = dstRef = dstDoc.createObject()
		dstRef.writeObject(graftObject(dstDoc, srcDoc, srcObj.resolve(), map))
		if (srcObj.isStream())
			dstRef.writeRawStream(srcObj.readRawStream())
		return dstRef
	}
	if (srcObj.isArray()) {
		dstObj = dstDoc.newArray()
		srcObj.forEach(function (key, val) {
			dstObj[key] = graftObject(dstDoc, srcDoc, val, map)
		})
		return dstObj
	}
	if (srcObj.isDictionary()) {
		dstObj = dstDoc.newDictionary()
		srcObj.forEach(function (key, val) {
			dstObj[key] = graftObject(dstDoc, srcDoc, val, map)
		})
		return dstObj
	}
	return srcObj /* primitive objects are not bound to a document */
}


function copyPage(dstDoc, srcDoc, pageNumber, map) {
	var srcPage, dstPage, offset	
	srcPage = srcDoc.findPage(pageNumber)
	dstPage = dstDoc.newDictionary()
	dstPage.Type = dstDoc.newName("Page")
	
	if (srcPage.MediaBox) dstPage.MediaBox = graftObject(dstDoc, srcDoc, srcPage.MediaBox, map)
	if (srcPage.Rotate) dstPage.Rotate = graftObject(dstDoc, srcDoc, srcPage.Rotate, map)
	if (srcPage.Resources) dstPage.Resources = graftObject(dstDoc, srcDoc, srcPage.Resources, map)
	if (srcPage.Contents) dstPage.Contents = graftObject(dstDoc, srcDoc, srcPage.Contents, map)

	dstDoc.insertPage(-1, dstDoc.addObject(dstPage))
}

function impose() {
	var imposed, cover, objects

	imposed = new PDFDocument()

  	cover = new PDFDocument('cover.pdf')
	objects = new PDFDocument('objects.pdf')

	print('Mixing cover and objects')

	copyPage(imposed, objects, 17, []);
	copyPage(imposed, cover, 0, []);
	
	copyPage(imposed, cover, 1, []);
	copyPage(imposed, objects, 16, []);

	copyPage(imposed, objects, 15, []);
	copyPage(imposed, objects, 0, []);

	copyPage(imposed, objects, 14, []);
	copyPage(imposed, objects, 1, []);

	copyPage(imposed, objects, 13, []);
	copyPage(imposed, objects, 2, []);

	copyPage(imposed, objects, 12, []);
	copyPage(imposed, objects, 3, []);

	copyPage(imposed, objects, 11, []);
	copyPage(imposed, objects, 4, []);

	copyPage(imposed, objects, 10, []);
	copyPage(imposed, objects, 5, []);

	copyPage(imposed, objects, 9, []);
	copyPage(imposed, objects, 6, []);

	copyPage(imposed, objects, 8, []);
	copyPage(imposed, objects, 7, []);

	print('Writing cover')
	imposed.save('reordered.pdf', 'compress')
	
}

impose()
