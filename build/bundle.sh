#! /bin/bash

echo 'Generating PDFS...'
#OSPKitPDF "http://192.168.1.11/html2print/chapters/cover.html" cover.pdf
#OSPKitPDF "http://192.168.1.11/html2print/chapters/objects.html" objects.pdf
#OSPKitPDF "http://192.168.1.11/html2print/chapters/systems.html" systems.pdf

echo "Rotating systems"
pdftk systems.pdf cat 1-endwest output systems-rotated.pdf

echo 'Normal PDF...'
mutool run reorder.js

echo 'Running nup...'
pdfnup --nup 2x1 --paper a3paper reordered.pdf

mutool run mix.js

echo 'Cleaning PDFs'
gs -sDEVICE=pdfwrite -dNOPAUSE -dBATCH -dAutoRotatePages=/None -sOutputFile=species.pdf species-mixed.pdf
