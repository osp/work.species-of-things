;(function(window, document) {
    'use strict';

    // http://stackoverflow.com/questions/8567114/how-to-make-an-ajax-call-without-jquery
    function callAjax(url, callback){
        var xmlhttp;
        // compatible with IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
        xmlhttp.withCredentials = true;
        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
                callback(xmlhttp.responseText);
            }
        }
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }

    // loads stories
    window.addEventListener("load", function () {
        var styles = document.head.querySelectorAll('etherstyle');

        for (var i = 0; i ; i ++) {
            (function () {
              callAjax(src, function(data) {
                  var style = document.createElement('style');
                  style.innerText = data
                  document.head.appendChild(style);
              });
            })();
        }
    });
})(window, document);