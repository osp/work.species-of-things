;(function(window, document) {
    'use strict';

    // http://stackoverflow.com/questions/8567114/how-to-make-an-ajax-call-without-jquery
    function callAjax(url, callback){
        var xmlhttp;
        // compatible with IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp = new XMLHttpRequest();
        xmlhttp.withCredentials = true;
        xmlhttp.onreadystatechange = function(){
            if (xmlhttp.readyState == 4 && xmlhttp.status == 200){
                callback(xmlhttp.responseText);
            }
        }
        xmlhttp.open("GET", url, true);
        xmlhttp.send();
    }

    // loads stories
    window.addEventListener("load", function () {
        var stories = document.querySelectorAll('article[data-src]'),
            loadedstories = 0;

        for (var i = 0, l = stories.length; i < l; i ++) {
            (function () {
            var story = stories[i];
            var src = story.dataset.src;

            callAjax(src, function(data) {
                story.innerHTML = data;
                loadedstories++;

                if (loadedstories == stories.length) {
                    var e = new Event("storiesloaded");
                    document.dispatchEvent(e);
                }
            });
            })();
        }
    });
})(window, document);