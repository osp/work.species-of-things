;(function(jQuery, $){
    // ________________________________ INIT __________________________________ //
    // Calculating the number of pages needed
    var content_height = $("#main").height();
    //var content_height = $("body").height();
    var body_height = $($("#master-page .body")[0]).height();
    var nb_page = 16;

    $master = $("#master-page");
    $container = $master.parent();
    $master.detach();

    //Cloning the master page
    for (i = 1; i <= nb_page; i++){
       $page = $master.clone().attr("id","page-"+i);
       $container.append($page);
    }
})(jQuery, $);

