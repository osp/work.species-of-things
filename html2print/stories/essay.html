<h1>Introduction</h1>

<p>
Design theory is often called a young discipline, emerging at the end of the 19th century with the rise of industrial manufacture. Yet objects as such have played an essential role in the lineage of written texts that establish both what is known about the world and how the unknown or indeterminate—religion, morality, cultural meaning, and phenomena of physical life—might be approached. This project is an exploration of how significant texts over the past two millennia have defined, contextualised, and categorised things in an attempt to make sense of their contemporary condition. By mapping each categorisation system, it is possible to compare them, putting into focus the unique motivation that gave order to each world-view. The category trees in the following texts are characterised by a variety of ontological forces, from the voracity of the collector to the specificity of the customs office, from the division of human labour in the Enlightenment to the organisation of mechanical power in the industrial era, from the consumerism of the online marketplace to the spiritual pursuit of the theologian. If we position design theory in a much longer lineage of diverse perspectives on the nature of things, we may find new territories for creation, recognise things that previously went unnoticed, and redefine the relationship of design to the things it produces and with which it produces the world around it.</p>

<h1>Notes on methodology</h1>
<p>
The seven classification systems used in this publication emerge from documents of diverse formats. In the case of Polyhistor, chapter headings have been translated into top-level categories, and the things described in those chapters are listed within them; the degree of nesting depends on the way Solinus describes things of similar ilk in relation to one another. In the case of Etymologiae and Great Inventions, chapter headings and sub-headings have been translated into top-level categories and sub-categories, and the things describes under those sub-headings have been listed with affinity for each author’s language in terms of nesting. In the case of the Cyclopaedia and the Encyclopédie, an overall categorisation system is given as a diagram, accompanied by an abbreviated list of sub-categories for each term. The actual body of each encyclopaedia constitutes an enormous number of entries that can be associated to the sub-categories by interpretation. In the case of the Combined Nomenclature TARIC code and the Amazon Product Categories, each categorisation system and all of its subcategories and entries is explicitly defined.
</p>
<p>
Each classification system has also been edited for length. In the case of Polyhistor, Etymologiae, the Cyclopaedia and the Encyclopédie, the editor has given a subjective interpretation of what is a thing and what is not. One criteria is the degree of human agency and intention in the understanding of a given thing. A “thing” is therefore something that has been recognised as such and possessed, manipulated or used in some way towards human benefit. By that logic, a mountain or a river is less of a thing than an agricultural field or a building; a wild animal is less of a thing than livestock, a domesticated or captive animal, or an animal used for its parts; a general or a king is less of a thing than a slave (who are explicitly described as possessions or things in Polyhistor, Etymologiae—which calls it the “most extreme of all evils”—and the Cyclopaedia; the Encyclopédie defines the slave as a possession while deeming the practice incompatible with the natural rights of man). In the case of TARIC and Amazon, their considerable length—tens of thousands of entries per system—has required severe reduction. Only a small number of specific categories and objects of interest have been followed through to their endpoints, in order to show both the breadth of top-level categories and the granularity and nesting within particular categories.
</p>

<h1>Classification Systems</h1>

<p>
    <div><span class="tag title">Title</span> Polyhistor</div>

    <div><span class="tag">Subtitle</span>  Containing the noble actions of humane creatures, the secrets and providence of nature, the description of countries, the manners of the people: with many marvelous things and strange antiquities, serving for the benefit and recreation of all sorts of persons</div>

    <div><span class="tag">Author</span>  Gaius Julius Solinus</div>

    <div><span class="tag">Dedication</span>  Oclatinius Adventus</div>

    <div><span class="tag">Year</span>  250</div>

    <div><span class="tag">Place</span>  Rome, Italy</div>

    <div><span class="tag">Translation</span>  Arthur Golding</div>

    <div><span class="tag">Year</span>  1587</div>

    <div><span class="tag">Publisher</span>  London: I. Charlewoode for Thomas Hacket</div>

</p>
<p>
    <div><span class="tag title">Title</span>  Etymologiae</div>

    <div><span class="tag">Author</span>  Isidore</div>

    <div><span class="tag">Dedication</span>  Braulius</div>

    <div><span class="tag">Year</span>  625</div>

    <div><span class="tag">Place</span>  Seville, Spain</div>

    <div><span class="tag">Translation</span>  Stephen A. Barney, W. J. Lewis, J. A. Beach and Oliver Berghof</div>

    <div><span class="tag">Year</span>  2006</div>

    <div><span class="tag">Publisher</span>  Cambridge: Cambridge University Press</div>

</p>
<p>
    <div><span class="tag title">Title</span> Cyclopaedia</div>

    <div><span class="tag">Subtitle</span> An Universal Dictionary of Arts and Sciences: Containing the Definitions of the Terms, and Accounts of the Things Signify'd Thereby, in the Several Arts, both Liberal and Mechanical, and the Several Sciences, Human and Divine: the Figures, Kinds, Properties, Productions, Preparations, and Uses, of Things Natural and Artificial; the Rise, Progress, and State of Things Ecclesiastical, Civil, Military, and Commercial: with the Several Systems, Sects, Opinions, etc; among Philosophers, Divines, Mathematicians, Physicians, Antiquaries, Criticks, etc.: The Whole Intended as a Course of Ancient and Modern Learning</div>

    <div><span class="tag">Author</span> Ephraim Chambers</div>

    <div><span class="tag">Dedication</span> King George II</div>

    <div><span class="tag">Year</span> 1728</div>

    <div><span class="tag">Place</span> London, England</div>

    <div><span class="tag">Publisher</span> Ephraim Chambers</div>

</p>
<p>

    <div><span class="tag title">Title</span> Encyclopédie</div>

    <div><span class="tag">Subtitle</span> A Systematic Dictionary of the Sciences, Arts, and Crafts</div>

    <div><span class="tag">Author</span> Denis Diderot and Jean le Rond d'Alembert</div>

    <div><span class="tag">Dedication</span> King Louis XV</div>

    <div><span class="tag">Year</span> 1751</div>

    <div><span class="tag">Place</span> Paris, France</div>

    <div><span class="tag">Publisher</span> André le Breton, Michel-Antoine David, Laurent Durand, and Antoine-Claude Briasson</div>

</p>
<p>

    <div><span class="tag title">Title</span> Great Inventions</div>

    <div><span class="tag">Subtitle</span> Volume XII of the Smithsonian Scientific Series</div>

    <div><span class="tag">Author</span> Charles Greeley Abbot</div>

    <div><span class="tag">Year</span> 1932</div>

    <div><span class="tag">Place</span> Washington, D.C., United States of America</div>

    <div><span class="tag">Publisher</span> New York: Smithsonian Institution </div>

</p>
<p>

    <div><span class="tag title">Title</span> Combined Nomenclature: Tarif Intégré Communautaire (TARIC) </div>

    <div><span class="tag">Subtitle</span> Council Regulation (EEC) No 2658/87</div>

    <div><span class="tag">Author</span> Council of the European Union</div>

    <div><span class="tag">Year</span> 1987</div>

    <div><span class="tag">Place</span> Brussels, Belgium</div>

    <div><span class="tag">Publisher</span> Official Journal of the European Communities

</p>
<p>

    <div><span class="tag title">Title</span> Amazon Product Categories</div>

    <div><span class="tag">Author</span> Amazon</div>

    <div><span class="tag">Year</span>  2017</div>

    <div><span class="tag">Place</span> Seattle, United States of America</div>

    <div><span class="tag">Publisher</span> Amazon</div>

</p>

<h1>Colophon</h1>
<p>
    <div>Editor: Tamar Shafrir</div>

    <div>Design: Open Source Publishing (Gijs de Heij) & Manetta Berends</div>
</p>
